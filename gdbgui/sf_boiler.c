/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: sf_boiler.c
 *
 * Code generated for Simulink model 'sf_boiler'.
 *
 * Model version                  : 1.85
 * Simulink Coder version         : 8.13 (R2017b) 24-Jul-2017
 * C/C++ source code generated on : Tue Jan 28 08:54:48 2020
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Intel->x86-64 (Linux 64)
 * Code generation objectives:
 *    1. Execution efficiency
 *    2. RAM efficiency
 * Validation result: Not run
 */

#include "sf_boiler.h"

/* Named constants for Chart: '<Root>/Bang-Bang Controller' */
#define GREEN                          ((int8_T)2)
static const uint8_T IN_HIGH                       = ((uint8_T)1U);
static const uint8_T IN_NORM                       = ((uint8_T)2U);
static const uint8_T IN_NO_ACTIVE_CHILD            = ((uint8_T)0U);
static const uint8_T IN_Off                        = ((uint8_T)1U);
static const uint8_T IN_On                         = ((uint8_T)2U);
#define OFF                            ((int8_T)0)
#define ON                             ((int8_T)1)
#define RED                            ((int8_T)1)

/* Private macros used by the generated code to access rtModel */
#ifndef rtmIsMajorTimeStep
# define rtmIsMajorTimeStep(rtm)       (((rtm)->Timing.simTimeStep) == MAJOR_TIME_STEP)
#endif

#ifndef rtmIsMinorTimeStep
# define rtmIsMinorTimeStep(rtm)       (((rtm)->Timing.simTimeStep) == MINOR_TIME_STEP)
#endif

#ifndef rtmGetTPtr
# define rtmGetTPtr(rtm)               ((rtm)->Timing.t)
#endif

#ifndef rtmSetTPtr
# define rtmSetTPtr(rtm, val)          ((rtm)->Timing.t = (val))
#endif

#ifndef UCHAR_MAX
#include <limits.h>
#endif

#if ( UCHAR_MAX != (0xFFU) ) || ( SCHAR_MAX != (0x7F) )
#error Code was generated for compiler with different sized uchar/char. \
Consider adjusting Test hardware word size settings on the \
Hardware Implementation pane to match your compiler word sizes as \
defined in limits.h of the compiler. Alternatively, you can \
select the Test hardware is the same as production hardware option and \
select the Enable portable word sizes option on the Code Generation > \
Verification pane for ERT based targets, which will disable the \
preprocessor word size checks.
#endif

#if ( USHRT_MAX != (0xFFFFU) ) || ( SHRT_MAX != (0x7FFF) )
#error Code was generated for compiler with different sized ushort/short. \
Consider adjusting Test hardware word size settings on the \
Hardware Implementation pane to match your compiler word sizes as \
defined in limits.h of the compiler. Alternatively, you can \
select the Test hardware is the same as production hardware option and \
select the Enable portable word sizes option on the Code Generation > \
Verification pane for ERT based targets, which will disable the \
preprocessor word size checks.
#endif

#if ( UINT_MAX != (0xFFFFFFFFU) ) || ( INT_MAX != (0x7FFFFFFF) )
#error Code was generated for compiler with different sized uint/int. \
Consider adjusting Test hardware word size settings on the \
Hardware Implementation pane to match your compiler word sizes as \
defined in limits.h of the compiler. Alternatively, you can \
select the Test hardware is the same as production hardware option and \
select the Enable portable word sizes option on the Code Generation > \
Verification pane for ERT based targets, which will disable the \
preprocessor word size checks.
#endif

#if 0

/* Skip this size verification because of preprocessor limitation */
#if ( ULONG_MAX != (0xFFFFFFFFFFFFFFFFUL) ) || ( LONG_MAX != (0x7FFFFFFFFFFFFFFFL) )
#error Code was generated for compiler with different sized ulong/long. \
Consider adjusting Test hardware word size settings on the \
Hardware Implementation pane to match your compiler word sizes as \
defined in limits.h of the compiler. Alternatively, you can \
select the Test hardware is the same as production hardware option and \
select the Enable portable word sizes option on the Code Generation > \
Verification pane for ERT based targets, which will disable the \
preprocessor word size checks.
#endif
#endif

#if 0

/* Skip this size verification because of preprocessor limitation */
#if ( ULLONG_MAX != (0xFFFFFFFFFFFFFFFFUL) ) || ( LLONG_MAX != (0x7FFFFFFFFFFFFFFFL) )
#error Code was generated for compiler with different sized ulong_long/long_long. \
Consider adjusting Test hardware word size settings on the \
Hardware Implementation pane to match your compiler word sizes as \
defined in limits.h of the compiler. Alternatively, you can \
select the Test hardware is the same as production hardware option and \
select the Enable portable word sizes option on the Code Generation > \
Verification pane for ERT based targets, which will disable the \
preprocessor word size checks.
#endif
#endif

/* Continuous states */
X rtX;

/* Block signals and states (auto storage) */
DW rtDW;

/* Real-time model */
RT_MODEL rtM_;
RT_MODEL *const rtM = &rtM_;
extern real_T rt_roundd_snf(real_T u);

/* private model entry point functions */
extern void sf_boiler_derivatives(void);

/* Forward declaration for local functions */
static void flash_LED(void);
static boolean_T cold(const int8_T *fixPttempdegC);
static boolean_T warm(const int8_T *fixPttempdegC);
static void turn_boiler(int8_T mode);

/*
 * This function updates continuous states using the ODE3 fixed-step
 * solver algorithm
 */
static void rt_ertODEUpdateContinuousStates(RTWSolverInfo *si )
{
  /* Solver Matrices */
  static const real_T rt_ODE3_A[3] = {
    1.0/2.0, 3.0/4.0, 1.0
  };

  static const real_T rt_ODE3_B[3][3] = {
    { 1.0/2.0, 0.0, 0.0 },

    { 0.0, 3.0/4.0, 0.0 },

    { 2.0/9.0, 1.0/3.0, 4.0/9.0 }
  };

  time_T t = rtsiGetT(si);
  time_T tnew = rtsiGetSolverStopTime(si);
  time_T h = rtsiGetStepSize(si);
  real_T *x = rtsiGetContStates(si);
  ODE3_IntgData *id = (ODE3_IntgData *)rtsiGetSolverData(si);
  real_T *y = id->y;
  real_T *f0 = id->f[0];
  real_T *f1 = id->f[1];
  real_T *f2 = id->f[2];
  real_T hB[3];
  int_T i;
  int_T nXc = 1;
  rtsiSetSimTimeStep(si,MINOR_TIME_STEP);

  /* Save the state values at time t in y, we'll use x as ynew. */
  (void) memcpy(y, x,
                (uint_T)nXc*sizeof(real_T));

  /* Assumes that rtsiSetT and ModelOutputs are up-to-date */
  /* f0 = f(t,y) */
  rtsiSetdX(si, f0);
  sf_boiler_derivatives();

  /* f(:,2) = feval(odefile, t + hA(1), y + f*hB(:,1), args(:)(*)); */
  hB[0] = h * rt_ODE3_B[0][0];
  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0]);
  }

  rtsiSetT(si, t + h*rt_ODE3_A[0]);
  rtsiSetdX(si, f1);
  sf_boiler_step();
  sf_boiler_derivatives();

  /* f(:,3) = feval(odefile, t + hA(2), y + f*hB(:,2), args(:)(*)); */
  for (i = 0; i <= 1; i++) {
    hB[i] = h * rt_ODE3_B[1][i];
  }

  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0] + f1[i]*hB[1]);
  }

  rtsiSetT(si, t + h*rt_ODE3_A[1]);
  rtsiSetdX(si, f2);
  sf_boiler_step();
  sf_boiler_derivatives();

  /* tnew = t + hA(3);
     ynew = y + f*hB(:,3); */
  for (i = 0; i <= 2; i++) {
    hB[i] = h * rt_ODE3_B[2][i];
  }

  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0] + f1[i]*hB[1] + f2[i]*hB[2]);
  }

  rtsiSetT(si, tnew);
  rtsiSetSimTimeStep(si,MAJOR_TIME_STEP);
}

real_T rt_roundd_snf(real_T u)
{
  real_T y;
  if (fabs(u) < 4.503599627370496E+15) {
    if (u >= 0.5) {
      y = floor(u + 0.5);
    } else if (u > -0.5) {
      y = u * 0.0;
    } else {
      y = ceil(u - 0.5);
    }
  } else {
    y = u;
  }

  return y;
}

/* Function for Chart: '<Root>/Bang-Bang Controller' */
static void flash_LED(void)
{
  /* MATLAB Function 'flash_LED': '<S1>:4' */
  /* Graphical Function 'flash_LED': '<S1>:4' */
  /* '<S1>:21:1' */
  if (rtDW.LED == OFF) {
    /* '<S1>:29:1' */
    rtDW.LED = rtDW.color;
  } else {
    /* '<S1>:13:1' */
    rtDW.LED = OFF;
  }
}

/* Function for Chart: '<Root>/Bang-Bang Controller' */
static boolean_T cold(const int8_T *fixPttempdegC)
{
  /* Constant: '<Root>/temperature set point' */
  /* MATLAB Function 'cold': '<S1>:6' */
  /* Graphical Function 'cold': '<S1>:6' */
  /* '<S1>:22:1' */
  return *fixPttempdegC * 25 <= 2240;
}

/* Function for Chart: '<Root>/Bang-Bang Controller' */
static boolean_T warm(const int8_T *fixPttempdegC)
{
  boolean_T b;

  /* MATLAB Function 'warm': '<S1>:9' */
  /* Graphical Function 'warm': '<S1>:9' */
  /* '<S1>:28:1' */
  b = !cold(fixPttempdegC);
  return b;
}

/* Function for Chart: '<Root>/Bang-Bang Controller' */
static void turn_boiler(int8_T mode)
{
  /* MATLAB Function 'turn_boiler': '<S1>:5' */
  /* Graphical Function 'turn_boiler': '<S1>:5' */
  /* '<S1>:15:1' */
  if (mode == ON) {
    /* '<S1>:17:1' */
    rtDW.color = GREEN;
  } else {
    /* '<S1>:18:1' */
    rtDW.color = RED;
  }

  /* '<S1>:19:1' */
  rtDW.LED = rtDW.color;

  /* '<S1>:19:1' */
  rtDW.boiler = mode;
}

/* Model step function */
void sf_boiler_step(void)
{
  boolean_T sf_internal_predicateOutput;
  real_T rtb_limit;
  int8_T fixPttempdegC;
  if (rtmIsMajorTimeStep(rtM)) {
    /* set solver stop time */
    rtsiSetSolverStopTime(&rtM->solverInfo,((rtM->Timing.clockTick0+1)*
      rtM->Timing.stepSize0));
  }                                    /* end MajorTimeStep */

  /* Update absolute time of base rate at minor time step */
  if (rtmIsMinorTimeStep(rtM)) {
    rtM->Timing.t[0] = rtsiGetT(&rtM->solverInfo);
  }

  /* Quantizer: '<S4>/quantize' incorporates:
   *  Gain: '<S4>/scale'
   *  Integrator: '<S2>/Integrator'
   *  Polyval: '<S3>/sensor'
   */
  rtb_limit = rt_roundd_snf((0.05 * rtX.Integrator_CSTATE + 0.75) * 51.2);

  /* Saturate: '<S4>/limit' */
  if (rtb_limit > 255.0) {
    rtb_limit = 255.0;
  } else {
    if (rtb_limit < 0.0) {
      rtb_limit = 0.0;
    }
  }

  /* End of Saturate: '<S4>/limit' */

  /* DataTypeConversion: '<S3>/Linear fixed point conversion' */
  fixPttempdegC = (int8_T)rtb_limit;
  if (rtmIsMajorTimeStep(rtM)) {
    /* Chart: '<Root>/Bang-Bang Controller' */
    if (rtDW.temporalCounter_i1 < 31U) {
      rtDW.temporalCounter_i1++;
    }

    if (rtDW.temporalCounter_i2 < 63U) {
      rtDW.temporalCounter_i2++;
    }

    /* Gateway: Bang-Bang
       Controller */
    /* During: Bang-Bang
       Controller */
    if (rtDW.is_active_c7_sf_boiler == 0U) {
      /* Entry: Bang-Bang
         Controller */
      rtDW.is_active_c7_sf_boiler = 1U;

      /* Entry Internal: Bang-Bang
         Controller */
      /* Transition: '<S1>:10' */
      rtDW.is_c7_sf_boiler = IN_Off;
      rtDW.temporalCounter_i2 = 0U;

      /* Entry 'Off': '<S1>:2' */
      turn_boiler(OFF);

      /* Entry Internal 'Off': '<S1>:2' */
      rtDW.temporalCounter_i1 = 0U;

      /* Entry 'Flash': '<S1>:57' */
      flash_LED();
    } else if (rtDW.is_c7_sf_boiler == IN_Off) {
      /* During 'Off': '<S1>:2' */
      sf_internal_predicateOutput = ((rtDW.temporalCounter_i2 >= 40U) && cold
        (&fixPttempdegC));
      if (sf_internal_predicateOutput) {
        /* Transition: '<S1>:12' */
        /* Exit Internal 'Off': '<S1>:2' */
        rtDW.is_c7_sf_boiler = IN_On;
        rtDW.temporalCounter_i1 = 0U;

        /* Entry 'On': '<S1>:3' */
        turn_boiler(ON);

        /* Entry Internal 'On': '<S1>:3' */
        switch (rtDW.was_On) {
         case IN_HIGH: {
          rtDW.is_On = IN_HIGH;
          rtDW.was_On = IN_HIGH;
          break;	
          }

         case IN_NORM: {
          rtDW.is_On = IN_NORM;
          rtDW.was_On = IN_NORM;
          break;	
          }

         default: {
          /* Transition: '<S1>:23' */
          rtDW.is_On = IN_HIGH;
          rtDW.was_On = IN_HIGH;
          break;	
          }
        }
      } else {
        /* During 'Flash': '<S1>:57' */
        if (rtDW.temporalCounter_i1 >= 5U) {
          /* Transition: '<S1>:58' */
          rtDW.temporalCounter_i1 = 0U;

          /* Entry 'Flash': '<S1>:57' */
          flash_LED();
        }
      }
    } else {
      /* During 'On': '<S1>:3' */
      if (rtDW.temporalCounter_i1 >= 20U) {
        /* Transition: '<S1>:11' */
        /* Exit Internal 'On': '<S1>:3' */
        rtDW.is_On = IN_NO_ACTIVE_CHILD;
        rtDW.is_c7_sf_boiler = IN_Off;
        rtDW.temporalCounter_i2 = 0U;

        /* Entry 'Off': '<S1>:2' */
        turn_boiler(OFF);

        /* Entry Internal 'Off': '<S1>:2' */
        rtDW.temporalCounter_i1 = 0U;

        /* Entry 'Flash': '<S1>:57' */
        flash_LED();
      } else {
        flash_LED();
        if (rtDW.is_On == IN_HIGH) {
          /* During 'HIGH': '<S1>:7' */
          if (warm(&fixPttempdegC)) {
            /* Transition: '<S1>:24' */
            rtDW.is_On = IN_NORM;
            rtDW.was_On = IN_NORM;
          }
        } else {
          /* During 'NORM': '<S1>:8' */
          if (warm(&fixPttempdegC)) {
            /* Transition: '<S1>:25' */
            rtDW.is_On = IN_NO_ACTIVE_CHILD;
            rtDW.is_c7_sf_boiler = IN_Off;
            rtDW.temporalCounter_i2 = 0U;

            /* Entry 'Off': '<S1>:2' */
            turn_boiler(OFF);

            /* Entry Internal 'Off': '<S1>:2' */
            rtDW.temporalCounter_i1 = 0U;

            /* Entry 'Flash': '<S1>:57' */
            flash_LED();
          }
        }
      }
    }

    /* End of Chart: '<Root>/Bang-Bang Controller' */

    /* Switch: '<S2>/Switch' incorporates:
     *  Constant: '<S2>/cooling rate'
     *  Constant: '<S2>/heating rate'
     *  DataTypeConversion: '<S2>/Data Type  Conversion1'
     */
    if (rtDW.boiler != 0) {
      rtb_limit = 1.0;
    } else {
      rtb_limit = -0.1;
    }

    /* End of Switch: '<S2>/Switch' */

    /* Gain: '<S2>/Gain3' */
    rtDW.Gain3 = 0.04 * rtb_limit;
  }

  if (rtmIsMajorTimeStep(rtM)) {
    rt_ertODEUpdateContinuousStates(&rtM->solverInfo);

    /* Update absolute time for base rate */
    /* The "clockTick0" counts the number of times the code of this task has
     * been executed. The absolute time is the multiplication of "clockTick0"
     * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
     * overflow during the application lifespan selected.
     */
    ++rtM->Timing.clockTick0;
    rtM->Timing.t[0] = rtsiGetSolverStopTime(&rtM->solverInfo);

    {
      /* Update absolute timer for sample time: [1.0s, 0.0s] */
      /* The "clockTick1" counts the number of times the code of this task has
       * been executed. The resolution of this integer timer is 1.0, which is the step size
       * of the task. Size of "clockTick1" ensures timer will not overflow during the
       * application lifespan selected.
       */
      rtM->Timing.clockTick1++;
    }
  }                                    /* end MajorTimeStep */
}

/* Derivatives for root system: '<Root>' */
void sf_boiler_derivatives(void)
{
  XDot *_rtXdot;
  _rtXdot = ((XDot *) rtM->derivs);

  /* Derivatives for Integrator: '<S2>/Integrator' */
  _rtXdot->Integrator_CSTATE = rtDW.Gain3;
}

/* Model initialize function */
void sf_boiler_initialize(void)
{
  /* Registration code */
  {
    /* Setup solver object */
    rtsiSetSimTimeStepPtr(&rtM->solverInfo, &rtM->Timing.simTimeStep);
    rtsiSetTPtr(&rtM->solverInfo, &rtmGetTPtr(rtM));
    rtsiSetStepSizePtr(&rtM->solverInfo, &rtM->Timing.stepSize0);
    rtsiSetdXPtr(&rtM->solverInfo, &rtM->derivs);
    rtsiSetContStatesPtr(&rtM->solverInfo, (real_T **) &rtM->contStates);
    rtsiSetNumContStatesPtr(&rtM->solverInfo, &rtM->Sizes.numContStates);
    rtsiSetNumPeriodicContStatesPtr(&rtM->solverInfo,
      &rtM->Sizes.numPeriodicContStates);
    rtsiSetPeriodicContStateIndicesPtr(&rtM->solverInfo,
      &rtM->periodicContStateIndices);
    rtsiSetPeriodicContStateRangesPtr(&rtM->solverInfo,
      &rtM->periodicContStateRanges);
    rtsiSetErrorStatusPtr(&rtM->solverInfo, (&rtmGetErrorStatus(rtM)));
    rtsiSetRTModelPtr(&rtM->solverInfo, rtM);
  }

  rtsiSetSimTimeStep(&rtM->solverInfo, MAJOR_TIME_STEP);
  rtM->intgData.y = rtM->odeY;
  rtM->intgData.f[0] = rtM->odeF[0];
  rtM->intgData.f[1] = rtM->odeF[1];
  rtM->intgData.f[2] = rtM->odeF[2];
  rtM->contStates = ((X *) &rtX);
  rtsiSetSolverData(&rtM->solverInfo, (void *)&rtM->intgData);
  rtsiSetSolverName(&rtM->solverInfo,"ode3");
  rtmSetTPtr(rtM, &rtM->Timing.tArray[0]);
  rtM->Timing.stepSize0 = 1.0;

  /* InitializeConditions for Integrator: '<S2>/Integrator' */
  rtX.Integrator_CSTATE = 15.0;
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */

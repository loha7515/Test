
#!/usr/bin/env python3
import fileinput
import sys

print("This program will path matlab-makefiles to use clang instead of gcc ")

if len(sys.argv) > 0:
    filename = str(sys.argv[1])
    print("Will now edit the following file: " + filename)
else:
    print("Please provide the path to the makefile as first argument")


# Read in the file
with open(filename, 'r') as file :
    filedata = file.read()
    newdata = filedata
    # Replace the compiler settings to change to clang
    newdata = newdata.replace("# C Compiler: GNU C Compiler", "# C Compiler: Clang Compiler")
    newdata = newdata.replace("CC = gcc","CC = clang")
    newdata = newdata.replace("# Linker: GNU Linker", "# Linker: Clang")
    newdata = newdata.replace("LD = gcc", "LD = clang")
    newdata = newdata.replace("# C++ Compiler: GNU C++ Compiler", "# C++ Compiler: Clang++ Compiler")
    newdata = newdata.replace("CPP = g++", "CPP = clang++")
    newdata = newdata.replace("# C++ Linker: GNU C++ Linker", "# C++ Linker: Clang Linker")
    newdata = newdata.replace("CPP_LD = g++", "CPP_LD = clang++")

    #configure debugging with -g
    newdata = newdata.replace(" = -c $(C_STANDARD_OPTS)"," = -g -c $(C_STANDARD_OPTS)")
    newdata = newdata.replace("= -c $(CPP_STANDARD_OPTS) ","= -g -c $(CPP_STANDARD_OPTS)")

    #prevent executable from being created in the parent directory
    newdata = newdata.replace("RELATIVE_PATH_TO_ANCHOR   = ..", "RELATIVE_PATH_TO_ANCHOR   = .")

# Write the file out again
with open(filename, 'w') as file:
  file.write(newdata)

print("Makefile reconfiguration completed ! ")

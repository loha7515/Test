#!/bin/sh
echo "Script to extract the blockdiagram needed for Model-Aware-Debugging from the simulink SLX file"
if [ -z "$1" ]
then
echo "Please provide slx file as first argument"
echo "example usage: ./extractFilesFromSLX.sh SIMULINK_FILE.slx"
exit
else
mkdir simulink
unzip $1 simulink/blockdiagram.xml
unzip $1 simulink/stateflow.xml
echo "you'll find the extracted blockdiagram.xlm in the xmlfiles subdirectory"
fi

#TODO: 
#fill the lldb_ModelAwareDebug_Plugin.conf dynamically with values
